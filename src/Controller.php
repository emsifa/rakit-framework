<?php namespace Rakit\Framework;

abstract class Controller {

	protected $app;

	public function __construct(App $app)
	{
		$this->app = $app;
	}

	public function __get($attr)
	{
		return $this->app->{$attr};
	}

	public function __set($attr, $value)
	{
		$this->app->{$attr} = $value;
	}

	public function __call($method, array $args)
	{
		return call_user_func_array(array($this->app, $method), $args);
	}

}