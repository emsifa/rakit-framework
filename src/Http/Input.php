<?php

namespace Rakit\Framework\Http;

class Input {

    protected $inputs = array();
    protected $files = array();

    public function __construct()
    {
        $inputs = (array) $_POST + (array) $_GET;
        $this->inputs = $inputs;

        $this->files = $_FILES;
    }

    public function all()
    {
        $inputs = $this->inputs;
        $files = array();
        foreach($files as $key => $value) {
            $files[$key] = $this->hasMultiFiles($key)? $this->multiFiles($key) : $this->file($key);
        }

        return array_merge($inputs, $files);
    }

    public function getPrefix($prefix)
    {
        $inputs = array();
        foreach($this->all() as $key => $value) {
            if(preg_match('/^'.$prefix.'/', $key)) {
                $inputs[preg_replace('/^'.$prefix.'/', '', $key)] = $value;
            }    
        }

        return $inputs;        
    }

    public function has($key)
    {
        return array_key_exists($key, $this->inputs);   
    }

    public function get($key, $default = null)
    {
        if( ! $this->has($key)) {
            return $default;
        }

        return is_string($this->inputs[$key])? trim($this->inputs[$key]) : $this->inputs[$key];
    }

    public function file($key)
    {
        if(!$this->hasFile($key)) return NULL;

        return $this->makeInputFile($this->files[$key]);
    }

    public function multiFiles($key)
    {  
        if(!$this->hasMultiFiles($key)) return array();

        $input_files = array();

        $files = $this->files[$key];

        $names = $files["name"];
        $types = $files["type"];
        $temps = $files["tmp_name"];
        $errors = $files["error"];
        $sizes = $files["size"];

        foreach($temps as $i => $tmp) {
            if(empty($tmp) OR !is_uploaded_file($tmp)) continue;

            $_file = array(
                'name' => $names[$i],
                'type' => $types[$i],
                'tmp_name' => $tmp,
                'error' => $errors[$i],
                'size' => $sizes[$i]
            );

            $input_files[] = $this->makeInputFile($_file);
        }

        return $input_files;
    }

    public function hasFile($key)
    {
        $file = @$this->files[$key];
        
        if(!$file) return FALSE;

        $tmp = $file["tmp_name"];

        if(!is_string($tmp)) return FALSE;

        return is_uploaded_file($tmp);
    }

    public function hasMultiFiles($key)
    {   
        $files = @$this->files[$key];

        if(!$files) return FALSE;

        $uploaded_files = $files["tmp_name"];
        if(!is_array($uploaded_files)) return FALSE;

        foreach($uploaded_files as $tmp_file) {
            if(!empty($tmp_file) AND is_uploaded_file($tmp_file)) return TRUE;
        }

        return FALSE;
    }

    protected function makeInputFile(array $_file)
    {
        return new InputFile($_file);
    }

}