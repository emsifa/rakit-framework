<?php

namespace Rakit\Framework;

use Rakit\Framework\App;
use Rakit\Event\Event;

class EventDispatcher extends Event {

    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function makeCallable($callable)
    {
        return $this->app->makeCallable($callable);
    }

}