<?php

namespace Rakit\Framework\Services\View;

class View {

    protected $data = array();
    protected $settings = array();
    protected $engine;

    public function __construct(ViewEngine $engine = null, array $settings = array())
    {
        if(!$engine) {
            $engine = new ViewEngine($this);
        }

        $this->engine = $engine;

        $this->settings = $settings;
    }

    public function getSettings()
    {
        return $this->settings;
    }

    public function getSetting($key, $default = null)
    {
        return isset($this->settings[$key])? $this->settings[$key] : $default;
    }

    public function setEngine(ViewEngine $engine)
    {
        $this->engine = $engine;
    }

    public function getEngine()
    {
        return $this->engine;
    }

    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function get($key, $default = null)
    {
        return isset($this->data[$key])? $this->data[$key] : $default;
    }

    public function render($file, array $data = array())
    {
        return $this->engine->render($file, $data);
    }

}