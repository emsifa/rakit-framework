<?php

namespace Rakit\Framework\Http;

use Rakit\Bag\Bag;

class HeaderBag extends Bag {

    public function __construct()
    {
        $headers = getallheaders();
        parent::__construct($headers);
    }

}