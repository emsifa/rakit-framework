<?php 

namespace Rakit\Framework\Router;

use Rakit\Router\Router as BaseRouter;
use ReflectionClass;
use ReflectionMethod;

class Router extends BaseRouter {

	public function annotation($controller)
	{	
		$reflections = new ReflectionClass($controller);

		$methods = $reflections->getMethods();

		foreach($methods as $method) {
			$route_data = $this->parseMethodAnnotation($method);

			if(!$route_data) continue;

			$route = $this->register($route_data['methods'], $route_data['uri']);
			foreach($route_data['conditions'] as $key => $regex) {
				$route->where($key, $regex);
			}

			foreach($route_data['before'] as $before) {
				$route->before($before);
			}

			foreach($route_data['after'] as $after) {
				$route->after($after);
			}

			if($route_data['name']) {
				$route->name($route_data['name']);
			}

			$route->action($controller.'@'.$method->getName());
		}
	}

	protected function parseMethodAnnotation(ReflectionMethod $method)
	{
		$doc = $method->getDocComment();

		$route_data = array(
			'methods' => null,
			'uri' => null,
			'conditions' => array(),
			'before' => array(),
			'after' => array()
		);

		preg_match_all('/\* \@(?<name>[a-zA-Z]+)([ \t]+)(?<value>[^\n]+)/', $doc, $match);
	
		$annotations = array();
		
		foreach($match[0] as $i => $m) {
			$name = $match['name'][$i];
			$value = $match['value'][$i];
			if( ! array_key_exists($name, $annotations)) {
				$annotations[$name] = array();
			}
			
			$annotations[$name][] = $value;
		}

		if(!array_key_exists('route', $annotations)) {
			return NULL;
		}

		$route_data = array_merge($route_data, $annotations);

		list($methods, $uri) = explode(' ', $annotations['route'][0], 2);

		if(!empty($annotations['name'])) $route_data['name'] = $annotations['name'][0];

		$route_data['methods'] = explode('|', $methods);
		$route_data['uri'] = $uri;
		$route_data['conditions'] = array();

		if(isset($annotations['param'])) {
			foreach($annotations['param'] as $annotation) {
				preg_match('/\$(?<param>[^ ]+)([ \t]+)?(?<regex>\/.*\/)?/', $annotation, $match);
				if(isset($match['regex'])) {
					$route_data['conditions'][$match['param']] = preg_replace('/(^\/|\/$)/', '', $match['regex']);
				}
			}
		}
		
		return $route_data;
	}

}