<?php

namespace Rakit\Framework;

use Closure;
use ArrayAccess;
use ReflectionObject;
use ReflectionProperty;

class Container implements ArrayAccess {

    const PROP_PROTECTED = 'PROTECT';
    const PROP_SINGLETON = 'SINGLETON';

    protected $properties = array();
    protected $protected_and_singletons = array();

    public function getProperties()
    {
        return $this->properties;
    }

    public function hasProp($prop)
    {
        return array_key_exists($prop, $this->properties);
    }

    public function hasOwnProperty($prop)
    {
        return property_exists($this, $prop);
    }

    public function singleton($prop, Closure $callable)
    {
        $this->properties[$prop] = function($app) use ($callable) {
            static $value;

            if(null === $value) {
                $value = $callable($app);
            }

            return $value;
        };

        $this->protected_and_singletons[$prop] = self::PROP_SINGLETON;
    }

    public function protect($prop, Closure $callable)
    {
        $this->properties[$prop] = function() use ($callable) {
            return $callable;
        };

        $this->protected_and_singletons[$prop] = self::PROP_PROTECTED;
    }

    public function isSingleton($prop)
    {
        return (
            array_key_exists($prop, $this->protected_and_singletons) 
            AND $this->protected_and_singletons[$prop] == self::PROP_SINGLETON
        );
    }

    public function isProtected($prop)
    {
        return (
            array_key_exists($prop, $this->protected_and_singletons) 
            AND $this->protected_and_singletons[$prop] == self::PROP_PROTECTED
        );
    }

    public function __get($prop)
    {   
        if( !$this->hasProp($prop) ) {
            return null;
        }

        $value = $this->properties[$prop];

        return $value instanceof Closure? $value($this) : $value;
    }

    public function __set($prop, $value)
    {
        $this->properties[$prop] = $value;

        if( array_key_exists($prop, $this->protected_and_singletons) ) {
            unset($this->protected_and_singletons[$prop]);
        }
    }

    public function __isset($prop)
    {
        return $this->hasProp($prop);
    }

    public function __unset($prop)
    {
        if($this->hasProp($prop)) { 
            unset($this->properties[$prop]);

            if(array_key_exists($prop, $this->protected_and_singletons)) {
                unset($this->protected_and_singletons[$prop]);
            }
        }
    }

    public function offsetSet($prop, $value) {
        return $this->__set($prop, $value);
    }

    public function offsetExists($prop) {
        return $this->__isset($prop);
    }

    public function offsetUnset($prop) {
        return $this->__unset($prop);
    }

    public function offsetGet($prop) {
        return $this->__get($prop);
    }

}