<?php

return array(

    'debug' => true,

    'router' => array(
        'route_class' => "Rakit\\Framework\\Router\\Route",
        'routemap_class' => "Rakit\\Framework\\Router\\RouteMap",
    ),

    'view' => array(
        'path' => '',
        'data' => array()
    )

);