<?php

namespace Rakit\Framework\Services\View;

use Rakit\Framework\App;
use Rakit\Framework\ServiceProviderInterface;

class ViewServiceProvider implements ServiceProviderInterface {

    public function boot(App $app)
    {
        $res = $app->response;

        $res->singleton('view', function($res) use ($app) {
            $view_settings = $app->config['view'];
            $engine = $app->config['view.engine'];

            $view = new View($engine, $view_settings);

            return $view;
        });

        $res->macro('view', function($file, array $data = array()) use ($res) {
            echo $res->view->render($file, $data);
        });
    }

    public function run(App $app)
    {

    }

}