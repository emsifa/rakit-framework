<?php

namespace Rakit\Framework\Http;

use Rakit\Framework\Container;
use Rakit\Framework\App;
use Rakit\Bag\Bag;
use Rakit\Framework\MacroableTrait;

class Response extends Container {

    use MacroableTrait;

    protected $http_status_messages = array(
        //Informational 1xx
        100 => '100 Continue',
        101 => '101 Switching Protocols',

        //Successful 2xx
        200 => '200 OK',
        201 => '201 Created',
        202 => '202 Accepted',
        203 => '203 Non-Authoritative Information',
        204 => '204 No Content',
        205 => '205 Reset Content',
        206 => '206 Partial Content',

        //Redirection 3xx
        300 => '300 Multiple Choices',
        301 => '301 Moved Permanently',
        302 => '302 Found',
        303 => '303 See Other',
        304 => '304 Not Modified',
        305 => '305 Use Proxy',
        306 => '306 (Unused)',
        307 => '307 Temporary Redirect',

        //Client Error 4xx
        400 => '400 Bad Request',
        401 => '401 Unauthorized',
        402 => '402 Payment Required',
        403 => '403 Forbidden',
        404 => '404 Not Found',
        405 => '405 Method Not Allowed',
        406 => '406 Not Acceptable',
        407 => '407 Proxy Authentication Required',
        408 => '408 Request Timeout',
        409 => '409 Conflict',
        410 => '410 Gone',
        411 => '411 Length Required',
        412 => '412 Precondition Failed',
        413 => '413 Request Entity Too Large',
        414 => '414 Request-URI Too Long',
        415 => '415 Unsupported Media Type',
        416 => '416 Requested Range Not Satisfiable',
        417 => '417 Expectation Failed',
        418 => '418 I\'m a teapot',
        422 => '422 Unprocessable Entity',
        423 => '423 Locked',

        //Server Error 5xx
        500 => '500 Internal Server Error',
        501 => '501 Not Implemented',
        502 => '502 Bad Gateway',
        503 => '503 Service Unavailable',
        504 => '504 Gateway Timeout',
        505 => '505 HTTP Version Not Supported'
    );

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->header = new Bag(array(
            'Content-Type' => 'text/html',
            'X-Powered-By' => 'Rakit framework'
        ));
        
        $this->reset();
    }

    /**
     * set http status code
     * 
     * @param int $status http status to set
     */
    public function setStatus($status)
    {
        $this->status = (int) $status;
        return $this;
    }

    /**
     * set response content type
     * 
     * @param string $type response content type
     */
    public function setContentType($type)
    {
        $this->header["CONTENT_TYPE"] = $type;
        return $this;
    }

    /**
     * get setted response content type
     *
     * @return string response content type
     */
    public function getContentType()
    {
        return $this->header["CONTENT_TYPE"];
    }

    /**
     * write file: append response body
     * 
     * @param string $str to append
     * @param boolean $reset set it to true will reset response body
     */
    public function write($str, $reset = false)
    {
        if($reset) $this->body = "";

        $this->body .= $str;

        return $this;
    }

    /**
     * set response body
     * 
     * @param string $content response body
     */
    public function setBody($content)
    {
        $this->body = $content;
        return $this;
    }

    /**
     * get response body
     *
     * @return string response body
     */
    public function getBody()
    {
        return $this->body;
    }

    public function json(array $data, $status = 200)
    {
        $json = json_encode($data);
        $this->setContentType("application/json");
        $this->setBody($json);
        return $this;
    }

    public function isJson()
    {
        return ($this->getContentType() == "application/json");
    }

    public function isHtml()
    {
        return ($this->getContentType() == "text/html");
    }

    public function notFound($callback = null)
    {
        $notfound_handler = $this->notfound_handler;

        $this->setStatus(404);
        
        if($notfound_handler) {
            $notfound_handler($this->app); 
        } else {
            $this->defaultNotFoundHandler($this->app);
        }

        $this->send();
    }

    public function setNotFoundHandler($callback)
    {
        $callback = $this->app->makeCallable($callback);
        
        if(!is_callable($callback)) {
            throw new \InvalidArgumentException("Not found handler must be callable");
        } 

        $this->protect('notfound_handler', function() use ($callback) {
            return $callback;
        });
    }

    public function getNotFoundHandler()
    {
        return $this->notfound_handler;
    }

    protected function defaultNotFoundHandler()
    {
        $this->setBody("Error 404: Not found");
    }

    public function reset()
    {
        $this->setContentType("text/html");
        $this->setStatus(200);
        $this->body = "";

        $this->has_sent = false;
        return $this;
    }

    public function send($output = null, $status = null)
    {
        if($output) {
            $this->body = $output;
        }

        if(is_int($status)) {
            $this->status = $status;
        }

        $this->createHeaders();

        $this->app->event->fire("response.before_send", $this, $this->app);

        echo $this->body;
        $this->has_sent = true;
        
        $this->app->event->fire("response.send", $this, $this->app);

        exit();
    }

    protected function createHeaders()
    {
        $headers = $this->header->all(false);

        // http://stackoverflow.com/questions/6163970/set-response-status-code
        header("HTTP/1.1 ".$this->http_status_messages[$this->status], true, $this->status);

        foreach($headers as $key => $value) {
            $header = $this->normalizeHeaderKey($key).': '.$value;
            header($header);
        }
    }

    // http://en.wikipedia.org/wiki/List_of_HTTP_header_fields#Response_fields
    protected function normalizeHeaderKey($key)
    {
        $not_ucwords = array(
            'P3P' => 'P3P', 
            'X XSS PROTECTION' => 'X-XSS-Protection', 
            'X UA COMPATIBLE', 'X-UA-Compatible', 
            'X WEBKIT CSP' => 'X-WebKit-CSP',
            'WWW AUTHENTICATE' => 'WWW-Authenticate'
        );

        $key = strtoupper($key);
        $key = str_replace(array('-','_'), ' ', $key);

        if(array_key_exists($key, $not_ucwords)) {
            return $not_ucwords[$key];
        }

        $key = ucwords($key);
        $key = str_replace(' ', '-', $key);

        return $key;
    }

}