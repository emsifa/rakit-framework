<?php

namespace Rakit\Framework\Router;

use Rakit\Router\RouteMap as BaseRouteMap;

class RouteMap extends BaseRouteMap {

    public function before($action)
    {
        foreach($this->routes as $route) {
            $route->before($action);
        }

        return $this;
    }

    public function after($action)
    {
        foreach($this->routes as $route) {
            $route->after($action);
        }

        return $this;
    }

    public function on($event, $callback)
    {
        foreach($this->routes as $route) {
            $route->on($event, $callback);
        }

        return $this;
    }

    public function once($event, $callback)
    {
        foreach($this->routes as $route) {
            $route->once($event, $callback);
        }
        
        return $this;
    }

}