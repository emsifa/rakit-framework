<?php

namespace Rakit\Framework;

use Closure;

trait MacroableTrait {

    protected $macros = array();

    public function hasMacro($name)
    {
        return isset($this->macros[$name]);
    }
    
    public function macro($name, Closure $macro)
    {
        $this->macros[$name] = $macro;
    }
    
    public function __call($method, $params) {
        if($this->hasMacro($method)) {
            return call_user_func_array($this->macros[$method], $params);
        } else {
            $class = get_class($this);
            
            throw new \Exception("Call to undefined macro {$class}::{$method}");
        }
    }
    
}