<?php

namespace Rakit\Framework;

use Rakit\Bag\Bag;

class Configurator extends Bag {

    public function loadDir($dir)
    {
        $files = scandir($dir);
        foreach($files as $file) {
            if("php" == pathinfo($file, PATHINFO_EXTENSION)) {
                $this->loadFile($dir."/".$file);
            }
        }
    }

    public function mergeDir($dir)
    {
        $configs = new static;
        $configs->loadDir($dir);

        $this_configs = $this->all(false);
        $merge_configs = $configs->all(false);

        $merged_configs = array_replace_recursive($this_configs, $merge_configs);

        $this->items = $merged_configs;
    }

    public function loadFile($file)
    {
        $filename = pathinfo($file, PATHINFO_FILENAME);
        $this->set($filename, require($file));
    }

    public function __set($namespace, $value)
    {
        $bag = new static;
        $this->namespaces[$namespace] = $bag;
       
        if(is_array($value)) {
            $bag->set($value, null);
        } elseif(is_string($value)) {
            $bag->loadPath($value);
        }
    }

}
