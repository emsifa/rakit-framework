<?php

namespace Rakit\Framework;

use Closure;
use Rakit\Util\Str;
use Rakit\Framework\Router\Router;
use Rakit\Framework\Container;
use Rakit\Framework\EventDispatcher;
use Rakit\Framework\Configurator;
use Rakit\Framework\Http\Request;
use Rakit\Framework\Http\Response;
use Rakit\Framework\ServiceProviderInterface;

use Rakit\Framework\Services\View\ViewServiceProvider;

class App extends Container {

    use MacroableTrait;

    protected $name;
    protected $booted = false;
    protected $middlewares = array();
    protected $services = array();
    protected $namespace_aliases = array();
    protected static $instances = array();

    public function __construct(array $configs = array())
    {
        $configs = $this->makeConfigs($configs);

        // setup configurator
        $this->singleton('config', function($app) use ($configs) {
            return new Configurator($configs);
        });

        $this->singleton('event', function($app) {
            return new EventDispatcher($app);
        });
        
        // setup router
        $this->singleton('router', function($app) {
            $router = new Router;
            $router->setRouteClass($app->config["router.route_class"]);
            $router->setRouteMapClass($app->config["router.routemap_class"]);

            return $router;
        });

        $this->register(new ViewServiceProvider);

        if( empty(static::$instances) ) {
            $this->setName('default');
        }
    }

    /**
     * setName
     *
     * Set application name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        static::$instances[$name] = $this;
    }

    /**
     * getName
     *
     * Get application name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * route
     *
     * Register new route
     *
     * @param string $method
     * @param string $path
     * @return Route
     */
    public function route($method, $path)
    {
        return $this->router->register($method, $path);
    }

    /**
     * get
     *
     * Register new GET route
     *
     * @param string $path
     * @return Route
     */
    public function get($path)
    {
        return $this->router->get($path);
    }

    /**
     * post
     *
     * Register new POST route
     *
     * @param string $path
     * @return Route
     */
    public function post($path)
    {
        return $this->router->post($path);
    }

    /**
     * put
     *
     * Register new PUT route
     *
     * @param string $path
     * @return Route
     */
    public function put($path)
    {
        return $this->router->put($path);
    }

    /**
     * patch
     *
     * Register new PATCH route
     *
     * @param string $path
     * @return Route
     */
    public function patch($path)
    {
        return $this->router->patch($path);
    }

    /**
     * delete
     *
     * Register new DELETE route
     *
     * @param string $path
     * @return Route
     */
    public function delete($path)
    {
        return $this->router->delete($path);
    }

    /**
     * group
     *
     * Grouping routes in a closure
     *
     * @param string $path
     * @param Closure $grouper
     * @return RouteMap
     */
    public function group($path, Closure $grouper)
    {
        return $this->router->group($path, $grouper);
    }

    /**
     * group
     *
     * Grouping routes in a closure
     *
     * @param array $routes
     * @return RouteMap
     */
    public function map(array $routes)
    {
        return $this->router->map($routes);
    }

    /**
     * register
     *
     * Register service
     *
     * @param ServiceProviderInterface $service
     */
    public function register(ServiceProviderInterface $service)
    {
        $this->services[] = $service;
    }


    /**
     * Register new DELETE route
     *
     * @param string $path
     * @return Route
     */
    public function add($middleware)
    {
        $callable = $this->makeCallable($middleware);

        if(!is_callable($callable)) {
            throw new \InvalidArgumentException("Middleware must be callable");
        }

        $this->middlewares[] = $callable;
    }

    /**
     * Detect the application's current environment.
     *
     * @param  array|string  $environments
     * @return string
     */
    public function detectEnvironment($environments)
    {
        if ($environments instanceof Closure) {
            return $this->config['env'] = call_user_func($environments);
        }

        if (php_sapi_name() == 'cli') {
            $arguments = $GLOBALS['argv'];
            $machine_env = $this->detectMachineEnvironment($environments);

            return $this->detectConsoleEnvironment($machine_env, $arguments);
        } else {
            $base = $_SERVER['HTTP_HOST'];

            return $this->detectWebEnvironment($base, $environments);
        }
    }

    /**
     * Set the application environment from command-line arguments.
     *
     * @param  array   $arguments
     * @return string
     */
    protected function detectMachineEnvironment(array $environments)
    {
        foreach ($environments as $environment => $machines) {
            foreach ($machines as $machine) {
                if (gethostname() == $machine) {
                    return $this->config['env'] = $environment;
                }
            }
        }

        return $this->config['env'] = 'production';
    }

    /**
     * Set the application environment for a web request.
     *
     * @param  string  $base
     * @param  array|string  $environments
     * @return string
     */
    protected function detectWebEnvironment($base, $environments)
    {
        foreach ($environments as $environment => $hosts) {
            foreach ($hosts as $host) {
                if (Str::is($host, $base) or gethostname() == $host) {
                    return $this->config['env'] = $environment;
                }
            }
        }

        return $this->config['env'] = 'production';
    }

    /**
     * Set the application environment from command-line arguments.
     *
     * @param  array   $arguments
     * @return string
     */
    protected function detectConsoleEnvironment($machine_env, array $arguments)
    {
        foreach ($arguments as $key => $value) {
            if (Str::startWith($value, '--env=')) {
                $segments = array_slice(explode('=', $value), 1);
                return $this->config['env'] = $segments[0];
            }
        }

        return $this->config['env'] = $machine_env;
    }

    /**
     * bootstraping application
     *
     * @param string $path custom uri path
     * @param string $method custom request method
     */
    public function boot($path = null, $method = null)
    {
        $this->event->fire("boot", $this);

        date_default_timezone_set($this->config->get('app.timezone', 'UTC'));

        if(!$path) {
            $path = isset($_SERVER['PATH_INFO'])? $_SERVER['PATH_INFO'] : '/';
        }

        $this->singleton('request', function($app) {
            return new Request($app);
        });

        $this->singleton('response', function($app) {
            return new Response($app);
        });

        $this->bootServices();

        $this->booted = true;
    }

    /**
     * run application
     *
     * @param string $path custom uri path
     * @param string $method custom request method
     */
    public function run($path = null, $method = null)
    {
        if(is_null($method)) {
            $method = $_SERVER['REQUEST_METHOD'];
        }

        if(!$path) {
            $path = str_replace($_SERVER['SCRIPT_NAME'], '', $_SERVER['REQUEST_URI']);
            $path = str_replace(dirname($_SERVER['SCRIPT_NAME']), '', $path);
            $path = strtok($path, '?');
        }

        if(!$this->booted) {
            $this->boot($path, $method);
        }

        $this->registerServices();

        $this->matched_route = $route = $this->router->routeMatch($path, $method);

        $this->event->fire("run", $this);

        if( !$route ) {
            return $this->response->notFound();
        } else {
            $this->request->defineRoute($route);

            $this->middlewares = array_merge($this->middlewares, $route->getActions());

            $route_events = $route->getEvents();
            foreach($route_events as $event) {
                $this->event->on($event['event'], $event['callback'], $event['limit']);
            }

            $this->input = $this->request->input;

            $this->response->reset(); 

            $this->next();

            $this->response->send();
        }
    }

    /**
     * getInstance
     *
     * getting application instance
     *
     * @param string $name
     * @return mixed (null|App)
     */
    public static function getInstance($name = 'default')
    {
        return static::$instances[$name];
    }

    /**
     * makeConfigs
     *
     * build app configuration
     *
     * @param array $configs
     * @return array
     */
    protected function makeConfigs(array $configs)
    {
        $default_configs = require(__DIR__."/default_configs.php");
        return array_merge($default_configs, $configs);
    }

    /**
     * boot registered services
     */
    protected function bootServices()
    {
        foreach($this->services as $service) {
            $service->boot($this);
        }
    }

    /**
     * register registered services
     */
    protected function registerServices()
    {   
        foreach($this->services as $service) {
            $service->run($this);
        }
    }

    /**
     * run next middleware/action
     *
     * @return string response body
     */
    public function next()
    {
        $action = array_shift($this->middlewares);

        if(!$action) return;

        $callable = $this->makeCallable($action);

        $args = $this->resolveArgs($callable);
        ob_start();
        call_user_func_array($callable, $args);
        $output = ob_get_clean();

        $this->response->write($output);

        $this->next();

        return $this->response->getBody();
    }

    /**
     * Add namespace alias for callable
     */
    public function addNamespaceAlias($namespace, $alias)
    {
        $namespace = Str::rootClass($namespace);
        $this->namespace_aliases[$alias] = $namespace;
    }

    /**
     * Transform string into array callable
     *
     * @return array|string|Closure callable
     */
    public function makeCallable($callable)
    {
        $rv = Str::REGEX_VARIABLE;
        $regex = "/^((?<alias>{$rv})\:)?(?<class>({$rv}(\\\|\/)?)+)\@(?<method>{$rv})/";

        if(is_string($callable) AND preg_match($regex, $callable, $match)) {
            $class = $match['class'];
            $alias = $match['alias'];
            $method = $match['method'];

            if(!empty($alias) AND array_key_exists($alias, $this->namespace_aliases)) {
                $class = $this->namespace_aliases[$alias].'/'.$class;
            }

            $class = str_replace('/', "\\", $class);
            $class = str_replace("\\\\", "\\", $class);

            if($this->hasProp($class)) {
                $obj = $this->{$class};
            } else {
                $obj = new $class($this);
            }

            return array($obj, $method);
        } else {
            return $callable;
        }
    }

    /**
     * Resolve arguments for middleware
     */
    protected function resolveArgs($callable)
    {
        $args = array();

        if(is_array($callable) AND $callable[0] instanceof Controller) {
            $args = array_merge($args, array_values($this->request->params));
        } else {
            $args[] = $this->request;
            $args[] = $this->response;
        }

        return $args;
    }

}