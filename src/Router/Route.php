<?php

namespace Rakit\Framework\Router;

use Rakit\Router\Route as BaseRoute;

class Route extends BaseRoute {

    protected $actions = array();

    protected $events = array();

    public function getActions()
    {
        return $this->actions;
    }

    public function getOnceEvents()
    {
        return $this->once_events();
    }

    public function getEvents()
    {
        return $this->events;
    }

    public function action($actions)
    {
        return $this->after($actions);
    }

    public function before($actions)
    {
        if( is_array($actions) ) {
            foreach($actions as $action) {
                $this->before($action);
            }

            return $this;
        } else {            
            array_unshift($this->actions, $actions);
        }
        
        return $this;
    }

    public function after($actions)
    {
        if( is_array($actions) ) {
            foreach($actions as $action) {
                $this->after($action);
            }

            return $this;
        } else {
           array_push($this->actions, $actions);
        }

        return $this;
    }

    public function on($event, $callback, $limit = 0)
    {
        $this->events[] = array(
            'event' => $event,
            'callback' => $callback,
            'limit' => $limit
        );

        return $this;
    }

}