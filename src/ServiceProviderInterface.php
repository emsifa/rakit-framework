<?php

namespace Rakit\Framework;

use Rakit\Framework\App;

interface ServiceProviderInterface {

    public function boot(App $app);
    public function run(App $app);

}