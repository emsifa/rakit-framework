<?php

namespace Rakit\Framework\Services\View;

use Exception;

class ViewEngine {

    protected $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function render($file, array $data = array())
    {
        $viewpath = $this->view->getSetting('path');

        $file = rtrim($viewpath,'/').'/'.$file;

        if(!file_exists($file)) {
            throw new Exception("Cannot render view file, '{$file}' not found");
        }

        $render = function($__file, $__data) {
            extract($__data);
            include($__file);
        };

        ob_start();
        $render($file, $data);        
        $rendered = ob_get_clean();

        return $rendered;
    }

}